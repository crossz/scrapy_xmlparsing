# -*- coding: utf-8 -*-

### method 1: using basic spider
#import scrapy
#from leidasports.items import LeidasportsItem
#
#class LdsxmlSpider(scrapy.Spider):
#    name = 'ldsxml'
#    start_urls = ['file:///tmp/odds_NBA.xml']
#    
#    def parse(self, response):
#        # print(response.body)
#    
#        selector = scrapy.Selector(response)
#        
#        item = LeidasportsItem()
#    
#        item['name'] = selector.xpath('//competition/name/text()').extract()
#        
#        print('-------------------------')
#        print(item)
#        print('=========================')
#        
#
#        yield item
        
        
    
## method 2: using XMLFeedSpider    
from scrapy.spiders import XMLFeedSpider
from leidasports.items import LeidasportsItem
import os

class LdsxmlSpider(XMLFeedSpider):
    name = 'ldsxml'
#    allowed_domains = ['example.com']
    #start_urls = ['file:///tmp/odds_NBA.xml']
    start_urls = ['file://' + os.getcwd()  + '/odds_NBA.xml']
#    iterator = 'iternodes'  # This is actually unnecessary, since it's the default value
#    namespaces = [('n', 'http://www.w3.org/2001/XMLSchema-instance')]
    itertag = 'competition'

    def parse_node(self, response, node):
#    def parse_node(self, response, selector):
#        self.logger.info('Hi, this is a <%s> node!: %s', self.itertag, ''.join(node.extract()))

        item = LeidasportsItem()
        
        item['name'] = node.xpath('name/text()').extract()
#        item['name'] = selector.xpath('//competition/name/text()').extract()

        print('-------------------------')
        print(item)
        print('=========================')
        
        yield item
        


### original: auto generated
#class LdsxmlSpider(scrapy.Spider):
#    name = 'ldsxml'
#    allowed_domains = ['example.com']
#    start_urls = ['http://example.com/']
#
#    def parse(self, response):
#        pass
